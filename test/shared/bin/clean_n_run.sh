#!/bin/bash

#/////////////////////////////////////////////
function separator
{ 
  line="-------------------------------"
  echo 
  echo "$line"
  echo "$1"
  echo "$line"
}
##############################################
function cleanup
{
  rm -vrf ./.yarn/cache
  rm -vrf ./.yarn/unplugged
}
#/////////////////////////////////////////////

separator "cleaning up"
cleanup

separator "re-install dependencies"
yarn install

separator "test result below"
node app.js
echo

