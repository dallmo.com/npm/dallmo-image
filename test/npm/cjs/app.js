"use strict";

/**
 * check loading images from folder
 */

const dallmo_image = require("dallmo-image");

// wrap all options into a single obj
const option_obj = {
  //file_input: "okok",
  //image_folder: "./inbox_2",
}; // option_obj

// main action
( async()=>{

  const result = await dallmo_image.to_webp( option_obj );
  console.log("cjs result : ", result );
  
})(); // async main

