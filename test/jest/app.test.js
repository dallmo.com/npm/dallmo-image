'use-strict';

// the app to be tested
const config_reader = require("../../dist/app");

// the sample config file
const sample_config_file = "../test/jest/sample-config.yaml";
const promise_config_read = config_reader( sample_config_file );

// testing async code, so "return" is needed
// ref : https://jestjs.io/docs/asynchronous
test(
  "test reading sample config from sample-config.yaml",
  () => {
    return promise_config_read.then(
      config_obj => {
        expect( config_obj.key1 ).toBe('value1');
        expect( config_obj.key2.key2a ).toBe('value2a');
    }); // promise
  });// test


