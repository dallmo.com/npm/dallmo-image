# dallmo-image

# overview
- a simple wrapper based on `sharp` for converting images, works mainly as a shortcut / alias with some frequently used parameters as default values ; 
- compatible with both cjs and esm, by following [the advice of a post by Dan Fabulich][ref-1]

[ref-1]: https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1

# usage

## prerequisities
1. add `etc/config.yaml` in the root where the app resides ; 
2. sample `config.yaml` :
```
---

# the folder to scan for input images
image_folder: "./images"

# the child folder inside image_folder for all output images
folder_output: "converted"

# number, for the method resize()
width: 800

# number, for the method webp()
quality: 60

# boolean, for the method toFile()
# placeholder only, not working as of 2023-08-26, version 0.1.1
# default : false
# - true : use timestamp down to ms as output filename
# - false : use original input filename, with file extension removed
file_output_timestamp: false
```

## cjs
```
"use strict";

const dallmo_image = require("dallmo-image");

// wrap all options into a single obj
const option_obj = {
  //file_input: "okok",
  //image_folder: "./inbox_2",
}; // option_obj

// main action
( async()=>{

  const result = await dallmo_image.to_webp( option_obj );
  console.log("cjs result : ", result );

})(); // async main
```

## esm
```
"use strict";

import {dallmo_image} from 'dallmo-image';

const option_obj = {};

const result = await dallmo_image.to_webp( option_obj );
      console.log( "esm result : ", result );
```


