"use strict";

/**
 * convert images in whatever format to webp
 * assumed options : 
 *  - same filename ( different extension )
 *  - quality : 60
 *  - width : 800
 * 
 * all of these can be overidden in the option_obj
 * default values listed above will be assumed if undefined
*/
//const fs = require("fs");
const fs = require( 'fs/promises' );
const path = require("path");
const dallmo_fs = require("dallmo-fs");

const sharp = require("sharp");
const config_file = "./etc/config.yaml";

////////////////////////////////////////////////////////////
async function get_config(){

  const dallmo_yaml = require("dallmo-yaml");
  
  // .................................
  let config_obj;
  try{
    config_obj = await dallmo_yaml( config_file );
    //console.log( "config_obj : ", config_obj );
  }catch(error){
    throw("ERROR : fail to read config file : ", config_file, "error message : ", error );
    console.error( error.message );
  }; // try catch, reading config file
  // .................................

    return config_obj;

}; // function get_config
////////////////////////////////////////////////////////////
// currently child folders inside the "converted" folder
// will be ignored
async function read_folder( folder_path ){

  //const fs = require( 'fs/promises' );
  const readdir = fs.readdir;

  console.log("reading folder path : ", folder_path );
  let image_array = [];

  try {
    const folder_items = await readdir( folder_path );
    for (const this_item of folder_items )
    {
      // need the relative path to check if this item is file or directory
      const this_item_fullpath = path.join( folder_path, this_item );

      // only keep files
      const is_file = (await fs.lstat( this_item_fullpath )).isFile();
      //console.log( "file_stat : ", file_stat );

      if( is_file ){
        image_array.push( this_item );
      }else{
        // call the function recursively maybe
        // works in upcoming versions
      }; // if isFile
    }// for
  }catch( error ){
    //console.error( error );
    throw new Error( error );
  }// try catch

  return image_array;

}; // function read_folder
////////////////////////////////////////////////////////////
async function convert_each( option_obj ){

  let result_convert;
  /*
  console.log("option_obj : ");
  console.dir( option_obj, { depth:null });
  */

  const file_input    = option_obj.file_input;
  const resize_option = option_obj.resize_option;
  const webp_option   = option_obj.webp_option;
  const file_output   = option_obj.file_output;

  try{
    result_convert = await sharp( file_input )
            .resize( resize_option )
            .webp( webp_option )
            .toFile( file_output );
            
  }catch(error){
    
    const error_message = "ERROR : failed to convert image via sharp.  file_input : " + file_input;
    console.error( error.message );
    console.log("the failed item comes with this option_obj : ", option_obj );

    const error_result_obj = {
      error_message: error_message,
    };// result_obj

    // return this for debug / keep as log entries
    result_convert = error_result_obj;

  }; // try catch, use sharp to convert image

    return result_convert;

}; // function convert_each
////////////////////////////////////////////////////////////
function remove_file_extension( filename ){

  // split the filename into an array
  const filename_array = filename.split("."); 
  
  // remove the last element and have the original array modified
  // the output result is only for debug here
  const filename_array_2 = filename_array.splice(-1,1); 
  
  // join the modified array back as a single string with the "." as separator
  const filename_no_ext = filename_array.join("."); 

  //console.log( filename_array, filename_array_2, filename_no_ext );
  
    return filename_no_ext;

}; // function remove_file_extension
////////////////////////////////////////////////////////////
// async for the mkdir operation
async function get_file_output( convert_all_option_obj, file_input ){

  // file_input_name_only : have original input filename only, without extension
  let file_input_name_only, file_output;
  /**
   * skip the timestamp as filename feature for now,
   * just take the original file name, remove the extension
   * and put it inside a folder named "converted" 
   */

  const file_extension = convert_all_option_obj.file_extension;
  const image_folder = convert_all_option_obj.image_folder;
  const folder_output = convert_all_option_obj.folder_output;
  const folder_output_fullpath = path.join( image_folder, folder_output );
    // make sure that folder exists
    await dallmo_fs.mkdir( folder_output_fullpath );

    file_input_name_only = remove_file_extension( file_input ).concat( file_extension ); // for now    
    file_output = path.join( folder_output_fullpath, file_input_name_only );
 
    return file_output;

}; // get_file_output
////////////////////////////////////////////////////////////
async function convert_all( convert_all_option_obj ){

  console.log( "convert_all_option_obj : " );
  console.dir( convert_all_option_obj, { depth: null } );

  const image_array = convert_all_option_obj.image_array;
  // throw error if no image to process
  if( image_array.length == 0 ){

    const error_message = "ERROR : dallmo-image : to_webp : no image to process. action aborted."
    console.error( error_message, "convert_all_option_obj : ", convert_all_option_obj );
    
    throw new Error( error_message );

  }; // if

  // else, move ahead
  let result_convert = [];
  const image_folder = convert_all_option_obj.image_folder;

  //.......................................................
  for( const each_image of image_array ){
    
    // get the relative path to the input image
    const file_input = path.join( image_folder, each_image );

    // get the relative path to the output image, with the file extension replaced
    const file_output = await get_file_output( convert_all_option_obj, each_image );

    const convert_each_option_obj = {
      file_input: file_input,
      resize_option: convert_all_option_obj.resize_option,
      webp_option: convert_all_option_obj.webp_option,
      file_output: file_output,
    }; // convert_each_option_obj

    const convert_each_result = await convert_each( convert_each_option_obj );
      result_convert.push( convert_each_result );

  }; // for
  //.......................................................

    return result_convert

}; // function convert
////////////////////////////////////////////////////////////
async function to_webp( option_obj ){

  let convert_result;

  // try catch errors here, and return the relevant messages just in case
  try{

  const file_extension = ".webp";

  // see if any one of these is not defined in the pass-in option_obj
  const undefined_file_output_timestamp     = ( option_obj.file_output_timestamp    == undefined );
  const undefined_file_input   = ( option_obj.file_input    == undefined );
  const undefined_image_folder = ( option_obj.image_folder == undefined );

  const undefined_quality  = ( option_obj.quality == undefined );
  const undefined_width    = ( option_obj.width   == undefined );

  let option_file_output_timestamp, option_file_input, option_image_folder, option_folder_output, 
      option_quality, option_width;
  let config_obj;

  // load config only if one of these are missing from option_obj
  if( undefined_file_output_timestamp || undefined_file_input || undefined_image_folder 
      || undefined_quality || undefined_width ){
    config_obj = await get_config();
  }; // 

  //console.log( "config_obj : ", config_obj );

  // ............................................
  // prepare the option vars for final actions
  // if the var is defined in the option_obj, use it
  // else, use the default values defined in the loaded config_obj
  ( undefined_image_folder ) ?
    ( option_image_folder = config_obj.image_folder ) : 
    ( option_image_folder = option_obj.image_folder );

  /**
   * if input_file is given, it has precedence over the image folder option
   *  
   * else, if input_file is not given in config_obj,
   * which means there is no inline para given as the input file,
   * then it should look for the "image_folder" option above
   */

  // just set it as-is for now
  option_folder_output = config_obj.folder_output;

  let image_array = [];
  if( undefined_file_input ){
    image_array = await read_folder( option_image_folder );
  }else{
    image_array.push( option_obj.file_input );
  }; // if, undefined_file_input 

  ( undefined_file_output_timestamp ) ? 
    ( option_file_output_timestamp = config_obj.file_output_timestamp ) : 
    ( option_file_output_timestamp = option_obj.file_output_timestamp );

  ( undefined_quality ) ? 
    ( option_quality = config_obj.quality ) : ( option_quality = option_obj.quality );

  ( undefined_width ) ?
    ( option_width = config_obj.width ) : ( option_width = option_obj.width );
  // ............................................

  /*
  console.log("final checks : ");
    console.log(" option_file_output_timestamp : ", option_file_output_timestamp );
    console.log(" option_quality :  ", option_quality );
    console.log(" option_width :  ", option_width );
  */
  
  // ............................................
  // define vars to pass to sharp
  const resize_option = {
    width: option_width
  }; // resize_option
  
  const webp_option = {
    quality: option_quality
  }; // webp_option

  // option obj for the main action
  const convert_all_option_obj = {
    file_extension: file_extension,
    file_input: option_file_input,
    folder_output: option_folder_output,
    image_folder: option_image_folder,
    image_array: image_array,
    resize_option: resize_option,
    webp_option: webp_option,
    file_output_timestamp: option_file_output_timestamp,
  }; // convert_all_option_obj

  // ............................................
  // main action
  convert_result = await convert_all( convert_all_option_obj );

}catch(error){

  console.error( error.message );
  convert_result = {
    error_message: error.message,
  }; // convert_result

}finally{
  return convert_result;
}; // try catch

}; // function to_webp
////////////////////////////////////////////////////////////
module.exports = to_webp;
////////////////////////////////////////////////////////////
